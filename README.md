# INDEC - ENTIC

## setting environment
After clone this project you should install the npm packages and bower packages.

    npm install -g bower
    npm install
    bower install

To run the app you should use this command.

    npm start

To install the app as a windows service you should use this command.

    node install

## updating applicacion
To update to the last version use:

    npm run update-web