var http = require('http'),
    path = require('path'),
    express = require('express'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser');

var app = global.app = express();

app.set('endpoint', 'https://entic.indec.gov.ar/mobile');

app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(express.static(path.join(__dirname, 'app')));

if (app.get('env') === 'development') {
    app.use(require('morgan')('dev'));
}

app.use(cookieParser());
app.use(bodyParser.json({limit: '50mb'}));

require('./routes')(app);

app.listen(3300, function (err) {
    if (err) {
        return console.log(err);
    }
    console.log('Server stated at port', 3300);
});