var fs = require('fs'),
    path = require('path'),
    exec = require('child_process').exec,
    request = require('request');

module.exports = function (router) {
    function loadUpdateMethod(callback) {
        request.get({
            url: app.get('endpoint') + '/ping',
            rejectUnauthorized: false
        }, function (err, response, body) {
            if (err || !response) {
                return callback(err);
            }
            fs.readFile(path.join(__dirname, '..\\..\\package.json'), function (err, packageFile) {
                if (err) {
                    return callback(err);
                }
                var update = {};
                var currentVersion = JSON.parse(packageFile).version.split('.');
                var pongVersion = JSON.parse(body).version.split('.');

                if (currentVersion[0] != pongVersion[0]) {
                    update.command = 'npm run-script update-service';
                    update.restart = true;
                } else if (currentVersion[1] != pongVersion[1]) {
                    update.command = 'npm run-script update-web-full';
                }
                else if (currentVersion[2] != pongVersion[2]) {
                    update.command = 'npm run-script update-web';
                }
                callback(null, update);
            });
        });
    }

    router.get('/update', function (req, res, next) {
        loadUpdateMethod(function (err, update) {
            if (err) {
                return res.sendStatus(503);
            }
            if (!update.command) {
                return res.send({updated: false});
            }
            var cwd = path.join(__dirname, '..\\..\\');
            // running the update script...
            exec(update.command, {cwd: cwd}, function (err, stdout, stderr) {
                if (err) {
                    return res.status(503).send({cwd: cwd, error: err, stdout: stdout, stderr: stderr});
                }
                if (update.restart) {
                    // restarting the windows service...
                    exec('node restart.js', {cwd: cwd});
                }
                // I should answer before stop the service.
                res.send({updated: true, restarting: update.restart});
            });
        });
    });

    router.post('/sync', function (req, res, next) {
        request.post({
            url: app.get('endpoint') + '/sync',
            rejectUnauthorized: false,
            gzip: true,
            auth: {
                username: req.user.username,
                password: req.user.password
            },
            form: {surveyAddresses: JSON.stringify(req.body.surveyAddresses)}
        }, function (err, response, body) {
            if (err) {
                return res.status(503).send(err);
            }
            if (response.statusCode == 200) {
                return res.send(body);
            }
            res.status(response.statusCode).send(body);
        });
    });

    router.post('/notifications/:id', function (req, res, next) {
        request.post({
            url: app.get('endpoint') + '/notifications/' + req.params.id,
            rejectUnauthorized: false,
            gzip: true,
            auth: {
                username: req.user.username,
                password: req.user.password
            },
            form: req.body
        }, function (err, response, body) {
            if (err || !response) {
                return res.send([]);
            }
            if (response.statusCode == 200) {
                return res.send(body);
            }
            res.status(response.statusCode).send(body);
        });
    });

    return router;
};