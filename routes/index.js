var express = require('express');

function authenticate(req, res, next) {
    if (req.cookies && req.cookies.userSession) {
        var userSession = req.cookies.userSession;
        if (typeof userSession == 'string') {
            userSession = JSON.parse(userSession);
        }
        if (userSession.id && userSession.username && userSession.password) {
            req.user = userSession;
            return next();
        }
    }
    if (req.xhr) {
        // If ajax.
        return res.json(403, {message: 'You don\'t have a session opened'});
    }
    res.redirect('/signin.html');
}

module.exports = function (router) {
    console.log('Loading public-api...');
    router.use('/public-api', require('./public-api')(express.Router()));

    console.log('Loading api...');
    router.use('/api', require('./api')(express.Router().use(authenticate)));
};
