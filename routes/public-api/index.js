var fs = require('fs'),
    path = require('path'),
    crypto = require('crypto'),
    request = require('request');

module.exports = function (router) {
    router.post('/sign-in', function (req, res, next) {
        if (!req.body.username || !req.body.password) {
            return res.sendStatus(400);
        }
        request.post({
            url: app.get('endpoint') + '/sign-in',
            rejectUnauthorized: false,
            form: {
                username: req.body.username,
                password: crypto.createHash('sha256').update(req.body.password).digest('base64')
            }
        }, function (err, response, body) {
            if (err || !response) {
                return res.status(503).send(err);
            }
            if (response.statusCode == 200) {
                return res.send(body);
            }
            res.status(response.statusCode).send(body);
        });
    });

    router.get('/ping', function (req, res, next) {
        request.get({
            url: app.get('endpoint') + '/ping',
            rejectUnauthorized: false
        }, function (err, response, body) {
            if (err || !response) {
                return res.status(503).send(err);
            }
            fs.readFile(path.join(__dirname, '..\\..\\package.json'), function (err, packageFile) {
                var pkg = JSON.parse(packageFile);
                var newVersion = JSON.parse(body).version;
                res.status(response.statusCode).send({
                    updateRequired: pkg.version != newVersion,
                    currentVersion: pkg.version,
                    newVersion: newVersion
                });
            });
        });
    });

    return router;
};
