var Service = require('node-windows').Service;

module.exports = function () {
    // Create a new service object
    return new Service({
        name: 'ENTIC',
        description: 'INDEC - Encuesta de TI y Comunicaciones.',
        script: require('path').join(__dirname, 'index.js')
    });
};
